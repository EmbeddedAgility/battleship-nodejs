const readline = require('readline-sync');
const gameController = require("./GameController/gameController.js");
const cliColor = require('cli-color');
const beep = require('beepbeep');
const position = require("./GameController/position.js");
const letters = require("./GameController/letters.js");


const chalkAnimation = require('chalk-animation');

chalkAnimation.rainbow('Lorem ipsum');
setTimeout(() => {
    // Stop the 'Lorem ipsum' animation, then write on a new line.
    console.log('dolor sit amet');
}, 1000);

function alertTerminal(){
    console.log("\007");
}

var player = require('play-sound')(opts = {})


colors = {
    'cyan' : '\x1b[36m%s\x1b[0m',

    'Reset': "\x1b[0m",
    'Bright': "\x1b[1m",
    'Dim': "\x1b[2m",
    'Underscore': "\x1b[4m",
    'Blink': "\x1b[5m",
    'Reverse': "\x1b[7m",
    'Hidden': "\x1b[8m",

    'BgBlack' : "\x1b[40m",
    'BgRed' : "\x1b[41m",
    'BgGreen' : "\x1b[42m",
    'BgYellow' : "\x1b[43m",
    'BgBlue' : "\x1b[44m",
    'BgMagenta' : "\x1b[45m",
    'BgCyan' : "\x1b[46m",
    'BgWhite' : "\x1b[47m",
    
    'FgBlack' : "\x1b[30m",
    'FgRed' : "\x1b[31m",
    'FgGreen' : "\x1b[32m",
    'FgYellow' : "\x1b[33m",
    'FgBlue' : "\x1b[34m",
    'FgMagenta' : "\x1b[35m",
    'FgCyan' : "\x1b[36m",
    'FgWhite' : "\x1b[37m",
    'FgOrange' : "\x1b[43m",
}

computerShoots = []

class Battleship {

    start() {
        console.log(cliColor.magenta("                                     |__"));
        console.log(cliColor.magenta("                                     |\\/"));
        console.log(cliColor.magenta("                                     ---"));
        console.log(cliColor.magenta("                                     / | ["));
        console.log(cliColor.magenta("                              !      | |||"));
        console.log(cliColor.magenta("                            _/|     _/|-++'"));
        console.log(cliColor.magenta("                        +  +--|    |--|--|_ |-"));
        console.log(cliColor.magenta("                     { /|__|  |/\\__|  |--- |||__/"));
        console.log(cliColor.magenta("                    +---------------___[}-_===_.'____                 /\\"));
        console.log(cliColor.magenta("                ____`-' ||___-{]_| _[}-  |     |_[___\\==--            \\/   _"));
        console.log(cliColor.magenta(" __..._____--==/___]_|__|_____________________________[___\\==--____,------' .7"));
        console.log(cliColor.magenta("|                        Welcome to Battleship                         BB-61/"));
        console.log(cliColor.magenta(" \\_________________________________________________________________________|"));
        console.log();

        this.InitializeGame();
        this.StartGame();
    }

    StartGame() {
        console.clear();
        console.log("                  __");
        console.log("                 /  \\");
        console.log("           .-.  |    |");
        console.log("   *    _.-'  \\  \\__/");
        console.log("    \\.-'       \\");
        console.log("   /          _/");
        console.log("  |      _  /");
        console.log("  |     /_\\'");
        console.log("   \\    \\_/");
        console.log("    \"\"\"\"");

        do {
            var isOutRange =false
            console.log();
            console.log("\x1b[34m","\bPlayer, it's your turn");
            console.log("\x1b[34m","\bEnter coordinates for your shot (e.g. A3,B5..) :");
            do{
                if (isOutRange){
                    console.log(colors.FgRed,"\b Miss! Position is out of range!!!");
                }
                var textInput = readline.question()
                    if (textInput != 'win') {
                        var position = Battleship.ParsePosition(textInput);
                } else {
                        this.processWin()
                }
                var position = Battleship.ParsePosition(textInput);
                isOutRange = !Battleship.CheckIsPositionInRange(position.row,position.column)
            }while(isOutRange)
            
            var isHit = gameController.CheckIsHit(this.enemyFleet, position);
            if (isHit) {
                beep();

                // We hit the boat
                console.log(colors.Reset);
                console.log(colors.FgBlue, "                \\         .  ./                ");
                console.log(colors.FgRed, "              \\      .:\";'.:..\"   /           ");
                console.log(colors.FgYellow, "                  (M^^.^~~:.'\").             ");
                console.log(colors.FgRed, "            -   (/  .    . . \\ \\)  -           ");
                console.log(colors.FgYellow, "               ((| :. ~ ^  :. .|))            ");
                console.log(colors.FgRed, "            -   (\\- |  \\ /  |  /)  -           ");
                console.log(colors.FgYellow, "                 -\\  \\     /  /-            ");
                console.log(colors.FgBlue, "                   \\  \\   /  /                ");
                console.log(colors.Reset);
                console.log(colors.FgGreen);
                console.log(colors.Underscore, "~~~~~~~~~~~~~~~    YOU HIT    ~~~~~~~~~~~~~~~~~");
                console.log(colors.Reset);

                if (gameController.CheckIfAllShpisAreSink(this.enemyFleet)){
                    console.log()
                    player.play('win.mp3', function(err){
                        if (err) throw err
                    })
                    console.log(colors.FgGreen,"You are the winner!");
                    process.exit()
                }

                
                console.log("Enemy ships defeated:")
                this.enemyFleet.forEach(function (ship) {
                    if (ship.isSinked==1){
                        console.log("Name: " + ship.name + " \t Size: " + ship.size)
                    }
                })
                console.log("=====================")
                console.log("Enemy ships left:")
                this.enemyFleet.forEach(function (ship) {
                    if (ship.isSinked!=1){
                        console.log("Name: " + ship.name + " \t Size: " + ship.size)
                    }
                })

            }

            console.log(isHit ? "\x1b[32m Yeah ! Nice hit !" : "\x1b[31m Miss");
            alertTerminal()

            var computerPos = this.GetRandomPosition();

            var isHit = gameController.CheckIsHit(this.myFleet, computerPos);
            console.log();
            console.log("\x1b[36m",`                           Computer shot in ${computerPos.column}${computerPos.row} and` + (isHit ? `\x1b[31m has hit your ship !` : `\x1b[32m miss`));
            if (isHit) {
                beep();

                // We got hit
                console.log(colors.Reset);
                console.log(colors.FgBlue, "                                \\         .  ./                ");
                console.log(colors.FgRed, "                           \\      .:\";'.:..\"   /           ");
                console.log(colors.FgYellow, "                                (M^^.^~~:.'\").             ");
                console.log(colors.FgRed, "                         -   (/  .    . . \\ \\)  -           ");
                console.log(colors.FgYellow, "                             ((| :. ~ ^  :. .|))            ");
                console.log(colors.FgRed, "                         -   (\\- |  \\ /  |  /)  -           ");
                console.log(colors.FgYellow, "                               -\\  \\     /  /-            ");
                console.log(colors.FgBlue, "                                 \\  \\   /  /                ");
                console.log(colors.Reset);
                console.log(colors.FgRed);
                console.log(colors.Underscore, "~~~~~~~~~~~~~~~~~~~~~~~  🏴‍☠️   YOU HAVE BEEN HIT  🏴‍☠️  ~~~~~~~~~~~~~~~~~~~~~~~~~~~");
                console.log(colors.Reset);
                if (gameController.CheckIfAllShpisAreSink(this.myFleet)){
                    console.log();
                    console.log(colors.FgRed,"You lost!");
                 
                    process.exit()
                } 

                console.log("Your ships defeated:")
                this.myFleet.forEach(function (ship) {
                    if (ship.isSinked==1){
                        console.log("Name: " + ship.name + " \t Size: " + ship.size)
                    }
                })

                console.log("=====================")
                console.log("Your ships left:")
                this.myFleet.forEach(function (ship) {
                    if (ship.isSinked!=1){
                        console.log("Name: " + ship.name + " \t Size: " + ship.size)
                    }
                })
            }
        }
        while (true);
    }

    static ParsePosition(input) {
        var letter = letters.get(input.toUpperCase().substring(0, 1));
        var number = parseInt(input.substring(1, 3), 10);
        return new position(letter, number);
    }

    GetAgain () {
        this.GetRandomPosition()
    }

    static CheckIsPositionInRange(inputNumber,inputLetter){

     
        if ((inputLetter!=undefined && inputLetter<9 ) && (inputNumber>0 && inputNumber<9 )){
            return true
        }
        
        return false
    }
    
    processWin () {
        console.log()
        player.play('win.mp3', function(err){
            if (err) throw err
        })
        
        console.log(colors.FgGreen,"You are the winner!");
        console.log(colors.Reset);
        console.log();
        console.log("          #########      ");
        console.log("        #############       ");
        console.log("        ##         ##      ");
        console.log("        #  ~~   ~~  #      ");
        console.log("       #  (.)   (.)  #      ");
        console.log("        (     ^     )      ");
        console.log("         |         |      ");
        console.log("         |  {===}  |      ");
        console.log("          \\       /       ");
        console.log("         /  -----  \\      ");
        console.log("     ----  |%\\ /%|  ---   ");
        console.log("     /     |%%%%%|     \\    ");
        console.log("           |%/ \\%|           ");
        console.log("");
        console.log(" Main minister of security:");
        console.log(" Congratulations! You have done the great job, whole nation is proud of you!");
        console.log(" You have choice now, you can continue to new mission or to be retired.");
        console.log(" Whole nation would be very happy if you continue to new mission,");
        console.log(" but we will also respect if you want to do something else.");
        console.log("");
        console.log("");
        console.log(" Do you want to continue to new mission? (y/n)");

        const input = readline.question();
        if (input == "y") {
            console.clear();
            console.log(colors.FgGreen, " The new adventure is starting now. Prepare you ships!");
            console.log();
            console.log();
            console.log();
            this.InitializeGame();
            this.StartGame();
        }
        else
        {
            console.log(" If you change your mind you are can always get back here.");

            console.log(colors.FgYellow, "        _...._");
            console.log(colors.FgYellow, "       _.dMMMMMMMMMb.");
            console.log(colors.FgYellow, "   ..dMMMMMMMMMMMMMMMb");
            console.log(colors.FgYellow, "  .dMMMMMMMMMMMMMMMMMMMMb.");
            console.log(colors.FgYellow, " dMMMMMMMMMMMMMMMMMMMMMMMM.");
            console.log(colors.FgYellow, " MMMMMMMP'`YMMMMMMMMMMMMMMMb");
            console.log(colors.FgYellow, " MMMMMMP    MMMMMMMMMMMMMMMM");
            console.log(colors.FgYellow, "dMMMMMP     `MMMMMMMMMMMMMMMb");
            console.log(colors.FgYellow, "MMMMMM~=,,_  `MMMMMMMMMMMMMMM");
            console.log(colors.FgYellow, "MMMMMMP,6;    `MMMMMMMMMMMMMMb");
            console.log(colors.FgYellow, "MMMMMM|         ```^YMMMMMMMMM");
            console.log(colors.FgYellow, "MMMMMM:   -~        / |MMMMMMMb");
            console.log(colors.FgYellow, "`MMMMM/\\  _.._     /  |MMMMMMMM");
            console.log(colors.FgYellow, "  `YMM\\_`.`~--'    \\__/MMMMMMMM!");
            console.log(colors.FgYellow, "   MMMMMM\\       _.' _sS}MMMMMb");
            console.log(colors.FgYellow, "    `YMMMMMb.__.sP.---.  MMMMMMM");
            console.log(colors.FgYellow, "     ``YMMMMMMMP'        \\MMMMMb");
            console.log(colors.FgYellow, "         ``MMMd;          MMMMMM");
            console.log(colors.FgYellow, "              dP|          :MMMMMb.");
            console.log(colors.FgYellow, "          _.sP'             :MMMMMM");
            console.log(colors.FgYellow, "      _.s888P'   ,  .-. .-. |MMMMM}");
            console.log(colors.FgYellow, "   .s888888P    ,_|(  you  )|MMMM");
            console.log(colors.FgYellow, " ..d88888888;     `\ `-._.-' ;;M'");
            console.log(colors.FgYellow, " 8888888888|       :         :;,");
            console.log(colors.FgYellow, " 8888888888;       |         |`;,_");
            console.log(colors.FgYellow, " `Y88888888b     _,:         |/Y\\;");
            console.log(colors.FgYellow, "    `^Y88888ssssSP~\":        ;SsP/");
            console.log(colors.FgYellow, "        \"\"\"\\        |         ;");
            console.log(colors.FgYellow, "            ;       |         |");
            console.log(colors.FgYellow, "            ;       ;         |");
            console.log(colors.FgYellow, "           /      .'          |");
            console.log(colors.FgYellow, "         .'    .-'            ;");
            console.log(colors.FgYellow, "        /_...-'             .'\\");
            console.log(colors.FgYellow, "       .'              _..-'   :");
            console.log(colors.FgYellow, "      /         __.--\"\"         :");
            console.log(colors.FgYellow, " ");
            console.log("Oh, thank you came back to me, YOU are my hero, I lovu you so much :D <3");
            console.log("");
            process.exit()
        }
    }

    GetRandomPosition() {
        var rows = 8;
        var lines = 8;
        let found = false
        var rndColumn = Math.floor((Math.random() * lines));
        var letter = letters.get(rndColumn + 1);
        var number = Math.floor((Math.random() * rows));
        var result = new position(letter, number);

        let location = letter.toString() + number.toString()

        computerShoots.forEach(element => {
            if (location == element) {
                // console.log(location + '=' + element)
                found = true
            }
        });

        // console.log(computerShoots)
        if (found) {
            found = false

            rows = 8;
            lines = 8;
            rndColumn = Math.floor((Math.random() * lines));
            letter = letters.get(rndColumn + 1);
            number = Math.floor((Math.random() * rows));
            result = new position(letter, number);

            location = letter.toString() + number.toString()
            return result;
        } else {
            computerShoots.push(location);
        }

        return result
    }

    InitializeGame() {
        this.InitializeEnemyFleet();
        this.InitializeMyFleet();
    }

    // InitializeMyFleet() {
    //     this.myFleet = gameController.InitializeShips();
    //     console.log("Please position your fleet (Game board size is from A to H and 1 to 8) :");

    //     this.myFleet.forEach(function (ship) {
    //         console.log();
        
    //         console.log(ship.color,`Please enter the positions for the ${ship.name} (size: ${ship.size})`);
    //         for (var i = 1; i < ship.size + 1; i++) {
    //             console.log(ship.color,`Enter position ${i} of ${ship.size} (i.e A3):`);
    //             const position = readline.question();
    //             ship.addPosition(Battleship.ParsePosition(position));
    //         }
    //     })
    // }

    InitializeMyFleet() {
        this.myFleet = gameController.InitializeShips();
        chalkAnimation.rainbow('Lorem ipsum dolor sit amet');
        console.log("🚢🚢🚢 Please position your fleet (Game board size is from A to H and 1 to 8) : 🚢🚢🚢");
        var rows = "ABCDEFGH";

        this.myFleet.forEach(function (ship, index) {
            console.log();
            console.log(`Do you want to auto fill: `);
            const input = readline.question();

            if(input == "yes" || input == "auto") { // auto fill the ship for me
                console.log("Entered test mode");
                for (var j = 0; j < ship.size; j++) {
                    console.log("POSITION " + rows.charAt(index) + j);
                    ship.addPosition(Battleship.ParsePosition(rows.charAt(index) + j));    
                }
            } else { // user entered ship coordinates

                for (var i = 1; i < ship.size + 1; i++) {
                    console.log(`Enter position ${i} of ${ship.size} (i.e A3):`);
                    var  isOutRange;
                    do {
                        var position = Battleship.ParsePosition(readline.question());
                        if(isOutRange = !Battleship.CheckIsPositionInRange(position.row,position.column)) {
                            console.log(colors.FgRed,"\bMiss! Position is out of range!!!");
                            console.log(colors.FgWhite, `\bEnter position ${i} of ${ship.size} (i.e A3):`);
                        }

                    } while(isOutRange)
                                     
                    ship.addPosition(position);
                }
            }
        })
    }


    InitializeEnemyFleet() {
        this.enemyFleet = gameController.InitializeShips();

        var number = Math.floor(Math.random() * (5 - 0)) + 0;

        this.generateShips(number);
    }

    generateShips(number) {
        switch(number) {
            case 0:
                this.enemyFleet[0].addPosition(new position(letters.B, 4));
                this.enemyFleet[0].addPosition(new position(letters.B, 5));
                this.enemyFleet[0].addPosition(new position(letters.B, 6));
                this.enemyFleet[0].addPosition(new position(letters.B, 7));
                this.enemyFleet[0].addPosition(new position(letters.B, 8));

                this.enemyFleet[1].addPosition(new position(letters.E, 6));
                this.enemyFleet[1].addPosition(new position(letters.E, 7));
                this.enemyFleet[1].addPosition(new position(letters.E, 8));
                this.enemyFleet[1].addPosition(new position(letters.E, 9));

                this.enemyFleet[2].addPosition(new position(letters.A, 3));
                this.enemyFleet[2].addPosition(new position(letters.B, 3));
                this.enemyFleet[2].addPosition(new position(letters.C, 3));

                this.enemyFleet[3].addPosition(new position(letters.F, 8));
                this.enemyFleet[3].addPosition(new position(letters.G, 8));
                this.enemyFleet[3].addPosition(new position(letters.H, 8));

                this.enemyFleet[4].addPosition(new position(letters.C, 5));
                this.enemyFleet[4].addPosition(new position(letters.C, 6));

                break;
            
            case 1: 
                this.enemyFleet[0].addPosition(new position(letters.A, 4));
                this.enemyFleet[0].addPosition(new position(letters.A, 5));
                this.enemyFleet[0].addPosition(new position(letters.A, 6));
                this.enemyFleet[0].addPosition(new position(letters.A, 7));
                this.enemyFleet[0].addPosition(new position(letters.A, 8));

                this.enemyFleet[1].addPosition(new position(letters.C, 5));
                this.enemyFleet[1].addPosition(new position(letters.C, 6));
                this.enemyFleet[1].addPosition(new position(letters.C, 7));
                this.enemyFleet[1].addPosition(new position(letters.C, 8));

                this.enemyFleet[2].addPosition(new position(letters.A, 2));
                this.enemyFleet[2].addPosition(new position(letters.B, 2));
                this.enemyFleet[2].addPosition(new position(letters.C, 2));

                this.enemyFleet[3].addPosition(new position(letters.E, 8));
                this.enemyFleet[3].addPosition(new position(letters.F, 8));
                this.enemyFleet[3].addPosition(new position(letters.G, 8));

                this.enemyFleet[4].addPosition(new position(letters.E, 5));
                this.enemyFleet[4].addPosition(new position(letters.F, 5));
                break;
            
            case 2:
                this.enemyFleet[0].addPosition(new position(letters.B, 4));
                this.enemyFleet[0].addPosition(new position(letters.B, 5));
                this.enemyFleet[0].addPosition(new position(letters.B, 6));
                this.enemyFleet[0].addPosition(new position(letters.B, 7));
                this.enemyFleet[0].addPosition(new position(letters.B, 8));

                this.enemyFleet[1].addPosition(new position(letters.D, 5));
                this.enemyFleet[1].addPosition(new position(letters.D, 6));
                this.enemyFleet[1].addPosition(new position(letters.D, 7));
                this.enemyFleet[1].addPosition(new position(letters.D, 8));

                this.enemyFleet[2].addPosition(new position(letters.A, 1));
                this.enemyFleet[2].addPosition(new position(letters.B, 1));
                this.enemyFleet[2].addPosition(new position(letters.C, 1));

                this.enemyFleet[3].addPosition(new position(letters.E, 7));
                this.enemyFleet[3].addPosition(new position(letters.F, 7));
                this.enemyFleet[3].addPosition(new position(letters.G, 7));

                this.enemyFleet[4].addPosition(new position(letters.E, 1));
                this.enemyFleet[4].addPosition(new position(letters.F, 1));
                break
            case 3:
                this.enemyFleet[0].addPosition(new position(letters.C, 4));
                this.enemyFleet[0].addPosition(new position(letters.C, 5));
                this.enemyFleet[0].addPosition(new position(letters.C, 6));
                this.enemyFleet[0].addPosition(new position(letters.C, 7));
                this.enemyFleet[0].addPosition(new position(letters.C, 8));

                this.enemyFleet[1].addPosition(new position(letters.E, 6));
                this.enemyFleet[1].addPosition(new position(letters.E, 7));
                this.enemyFleet[1].addPosition(new position(letters.E, 8));
                this.enemyFleet[1].addPosition(new position(letters.E, 9));

                this.enemyFleet[2].addPosition(new position(letters.E, 2));
                this.enemyFleet[2].addPosition(new position(letters.F, 2));
                this.enemyFleet[2].addPosition(new position(letters.G, 2));

                this.enemyFleet[3].addPosition(new position(letters.A, 1));
                this.enemyFleet[3].addPosition(new position(letters.B, 1));
                this.enemyFleet[3].addPosition(new position(letters.C, 1));

                this.enemyFleet[4].addPosition(new position(letters.C, 2));
                this.enemyFleet[4].addPosition(new position(letters.C, 3));

                break;
            
            case 4:
                this.enemyFleet[0].addPosition(new position(letters.G, 3));
                this.enemyFleet[0].addPosition(new position(letters.G, 4));
                this.enemyFleet[0].addPosition(new position(letters.G, 5));
                this.enemyFleet[0].addPosition(new position(letters.G, 6));
                this.enemyFleet[0].addPosition(new position(letters.G, 7));

                this.enemyFleet[1].addPosition(new position(letters.B, 7));
                this.enemyFleet[1].addPosition(new position(letters.C, 7));
                this.enemyFleet[1].addPosition(new position(letters.D, 7));
                this.enemyFleet[1].addPosition(new position(letters.E, 7));

                this.enemyFleet[2].addPosition(new position(letters.B, 3));
                this.enemyFleet[2].addPosition(new position(letters.B, 4));
                this.enemyFleet[2].addPosition(new position(letters.B, 5));

                this.enemyFleet[3].addPosition(new position(letters.D, 1));
                this.enemyFleet[3].addPosition(new position(letters.E, 1));
                this.enemyFleet[3].addPosition(new position(letters.F, 1));

                this.enemyFleet[4].addPosition(new position(letters.D, 5));
                this.enemyFleet[4].addPosition(new position(letters.E, 5));
                break;
            case 5:
                this.enemyFleet[0].addPosition(new position(letters.A, 3));
                this.enemyFleet[0].addPosition(new position(letters.A, 4));
                this.enemyFleet[0].addPosition(new position(letters.A, 5));
                this.enemyFleet[0].addPosition(new position(letters.A, 6));
                this.enemyFleet[0].addPosition(new position(letters.A, 7));

                this.enemyFleet[1].addPosition(new position(letters.B, 8));
                this.enemyFleet[1].addPosition(new position(letters.C, 8));
                this.enemyFleet[1].addPosition(new position(letters.D, 8));
                this.enemyFleet[1].addPosition(new position(letters.E, 8));

                this.enemyFleet[2].addPosition(new position(letters.C, 3));
                this.enemyFleet[2].addPosition(new position(letters.C, 4));
                this.enemyFleet[2].addPosition(new position(letters.C, 5));

                this.enemyFleet[3].addPosition(new position(letters.D, 6));
                this.enemyFleet[3].addPosition(new position(letters.E, 6));
                this.enemyFleet[3].addPosition(new position(letters.F, 6));

                this.enemyFleet[4].addPosition(new position(letters.D, 1));
                this.enemyFleet[4].addPosition(new position(letters.E, 1));
                break
        }   
    }

    ShipCoordinations () {
        var shipCoordinatio = {

        }
    }
}

module.exports = Battleship;