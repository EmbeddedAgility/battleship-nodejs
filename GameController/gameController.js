class GameController {
    static InitializeShips() {
        var colors = require("cli-color");
        const Ship = require("./ship.js");
        var ships = [
            new Ship("Aircraft Carrier", 5, "\x1b[34m",0),
            new Ship("Battleship", 4, "\x1b[31m",0),
            new Ship("Submarine", 3, "\x1b[1;32m",0),
            new Ship("Destroyer", 3, "\x1b[33m",0),
            new Ship("Patrol Boat", 2, "\x1b[0;33m",0)
        ];
        var sinkedShip
        return ships;
    }

    static CheckIsHit(ships, shot) {
        if (shot == undefined)
            throw "The shooting position is not defined";
        if (ships == undefined)
            throw "No ships defined";
        var returnvalue = false;
        ships.forEach(function (ship) {
            ship.positions.forEach(position => {
                if (position.row == shot.row && position.column == shot.column){
                    returnvalue = true;
                    position.isHit=1
                }
            });
        });
        return returnvalue;
    }

    static CheckIfAllShpisAreSink(ships){
        var isSink = true
        ships.forEach(function (ship) {
            if(!ship.checkIfShipIsSink()){
                isSink=false
            } else {
                ship.isSinked=1
            }
        })
        return isSink
    }
    static isShipValid(ship) {
        return ship.positions.length == ship.size;
    }
}

module.exports = GameController;