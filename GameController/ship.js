class Ship {
    constructor(name, size, color,isSinked) {
        this.name = name;
        this.size = size;
        this.color = color;
        this.isSinked=isSinked
        this.positions = [];
    }

    addPosition(position) {
        this.positions.push(position);
    }

    checkIfShipIsSink(){
        var isShipSink = true
        this.positions.forEach(position => {
            if (position.isHit==0) { isShipSink= false }
        })
        return isShipSink
        // console.log(this.positions)
    }
}

module.exports = Ship;